from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from tasks.models import Task
from projects.forms import CreateForm
# Create your views here.


def go_home(request):
    return redirect("home")


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "project_list": projects,
    }

    return render(request, "projects/projects.html", context)

@login_required
def show_project(request, id):
    projects = get_object_or_404(Project, id=id)
    context = {
        "project_object": projects,
    }

    return render(request, "projects/detail.html", context)

#@login_required
#def show_task(request, id):
    #tasks = get_object_or_404(Task, id=id)
    #context = {
    #    "task_object": tasks,
   # }

    #return render(request, "projects/detail.html", context)


def create_project(request):
    if request.method == "POST":
        form = CreateForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("list_projects")
    else:
        form = CreateForm()

    context = {
        "form": form,
    }

    return render(request, "projects/create.html", context)
