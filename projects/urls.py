from django.urls import path
from projects.views import list_projects, show_project, go_home, create_project
from tasks.views import create_task


urlpatterns = [
    path("", go_home, name="home"),
    path("<int:id>/", show_project, name="show_project"),
    path("", list_projects, name="list_projects"),
    path("create/", create_project, name="create_project"),
    path("tasks/create", create_task, name="create_task")
]
